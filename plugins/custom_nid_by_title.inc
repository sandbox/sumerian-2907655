<?php

/**
 * $file
 * Adds a nid by title plugin to the feeds tamper module.
 */

$plugin = array(
  'form' => 'custom_feeds_tamper_nid_by_title_form',
  'callback' => 'custom_feeds_tamper_nid_by_title_callback',
  'validate' => 'custom_feeds_tamper_nid_by_title_validate',
  'name' => '(Custom) nid by title',
  'category' => 'Custom',
);

function custom_feeds_tamper_nid_by_title_form($importer, $element_key, $settings) {
  $form = array();

  $form['title_field'] = array(
    '#type' => 'textfield',
    '#title' => t('Field name for title.'),
    '#default_value' => isset($settings['title_field']) ? $settings['title_field'] : "",
    '#description' => t('Enter the name of the field that contains node title in your source file.'),
    '#required' => TRUE,
  );

  $form['if_empty'] = array(
    '#type' => 'checkbox',
    '#title' => t('Only if nid is empty.'),
    '#description' => t('Do this only if nid value is empty in your source.'),
    '#default_value' => isset($settings['if_empty']) ? $settings['if_empty'] : FALSE,
  );

  return $form;
}

function custom_feeds_tamper_nid_by_title_validate(&$settings) {
  if (!$settings['title_field']) {
    form_set_error('settings][title_field', t('You must enter field name for title.'));
  }
}

function custom_feeds_tamper_nid_by_title_callback($result, $item_key, $element_key, &$field, $settings) {
  if ($settings['if_empty'] && !empty($field)) {
    return;
  }

  $title_field_name = mb_strtolower($settings['title_field']);

  if (!isset($result->current_item[$title_field_name])) {
    return;
  }

  $title = $result->current_item[$title_field_name];

  $nid = db_query("SELECT nid FROM {node} WHERE title = :title AND type = :type",
      array(
        ':title' => $title,
        ':type' => 'product_entity'
        ))->fetchField();

  if (!$nid) {
    return;
  }

  $field = $nid;
}

